#include <iostream>
#include <cmath>

int NumOfZeros(int N){
    int res = 0;
    int i = 1;
    while (N / floor(pow(5, i)) > 0){
        res += N / floor(pow(5, i));
        i++;
    }
    return res;
}

int main()
{
    std:: cout << NumOfZeros(100) << std::endl;

    return 0;
}
