#pragma once
#include <memory>
#include <vector>
#include <random>
#include <algorithm>

namespace Puzzle15 {

#define BOARD_SIZE 16
#define SIDE 4

class State
{
public:
    State(const int arr[BOARD_SIZE], int dist = 0);
    State(const std::vector<int> &vec, int dist = 0);

    void Print() const;
    void GetSuccessors(std::vector<std::shared_ptr<State> > &successors) const;
    void CopyAndSwapItems(int* arr, const size_t first, const size_t second) const;
    int CalculateHeuristic() const;
    bool IsTerminal() const;
    bool IsSolvable() const;
    bool Equals(const int* arr) const;
    bool Equals(std::shared_ptr<const State> other) const;
    void PrintPath() const;
    bool InPath() const;
    //Getters
    int GetDistance() const;
    std::shared_ptr<const State> GetParent() const;
    int GetItem(const size_t index) const;
    //Setters
    void SetParent(std::shared_ptr<const State> par);
    void SetDistance(int dist);

private:
    //Heuristics
    int ManhattanDist() const;
    int LinearConflict() const;
    int CornerConflict() const;

    std::shared_ptr<const State> parent;
    int config[BOARD_SIZE];
    int distance;
};

std::vector<int> GenerateRandomState(const int a);

}
