#include <iostream>
#include "state.h"
#include "beamsearch.h"


#define WIDTH 1000

using namespace Puzzle15;

int main()
{
    int arr[BOARD_SIZE] = {2,3,1,4,9,7,6,8,5,15,0,11,13,10,14,12};
    int term[BOARD_SIZE] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};
    int reverce[BOARD_SIZE] = {0,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
    State st(reverce, 0);

    st.Print();

    BeamSearch s(st, WIDTH);
    auto path = s.FindPath();
    std::cout << "BS Steps: " << path.size()-1 <<std::endl;
    return 0;
}
