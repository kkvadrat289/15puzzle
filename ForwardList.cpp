#include <iostream>

struct Node
{
    Node(int value):next(nullptr), value(value){}
    Node* next;
    int value;
};

class List
{
public:
    List():head(nullptr){}
    ~List(){
        if (head == nullptr)
            return;
        Node* current = head;
        Node* next = current->next;
        while (next != nullptr){
            delete(current);
            current = next;
            next = current->next;
        }
        delete(current);
    }
    void PrintList() const{
        if (head == nullptr){
            std::cout << "List is empty" << std::endl;
            return;
        }
        Node* current = head;
        std::cout << head->value;
        while (current->next != nullptr){
            current = current->next;
            std::cout << "-->" << current->value;
        }
        std::cout << std::endl;
    }
    void AddNodeByValue(const int value){
        Node* oldHead = head;
        head = new Node(value);
        if (oldHead != nullptr)
            head->next = oldHead;
    }

    void ReverceList(){
        if (head == nullptr)
            return;
        Node* next = head->next;
        Node* prev = nullptr;
        Node* current = head;
        while (next != nullptr){
            current->next = prev;
            prev = current;
            current = next;
            next = current->next;
        }
        current->next = prev;
        head = current;
    }
private:
    Node* head;
};

int main()
{
    List list;
    for (int i = 0; i < 18; ++i){
        list.AddNodeByValue(i);
    }

    list.PrintList();
    list.ReverceList();
    list.PrintList();

    return 0;
}
